# Twitter Java Spark Structured Streaming

## Goals

* Study data streaming process with Kafka
* Build data pipeline
* Consume [Twitter Streaming Kafka](https://gitlab.com/emersonnaka/twitter-streaming-kafka) application

## Requirements

* Java 8
* Maven

## Execution

* Main class: `src/main/java/com/nakashima/application/TwitterJavaSparkStructuredStreaming.java`
* To create a step to transform data, it's necessary create an implemented class Step and insert it into `buildStepList` method at `src/main/java/com/nakashima/application/config/ProcessPipeline.java`
`

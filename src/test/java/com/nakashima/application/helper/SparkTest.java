package com.nakashima.application.helper;

import org.apache.spark.SparkConf;
import org.apache.spark.SparkContext;
import org.apache.spark.sql.SparkSession;
import org.junit.jupiter.api.BeforeEach;

public interface SparkTest {

    SparkConf conf = new SparkConf()
            .setMaster("local[*]")
            .setAppName("Unit Test Data Frame")
            .set("spark.driver.host", "localhost");

    SparkSession sparkSession = SparkSession.builder()
            .sparkContext(new SparkContext(conf))
            .config("spark.sql.sources.partitionOverwriteMode", "dynamic")
            .config("spark.driver.allowMultipleContexts", "true")
            .getOrCreate();

    @BeforeEach
    default void setLoadData() {
        sparkSession.sparkContext().setLogLevel("ERROR");
    }

}

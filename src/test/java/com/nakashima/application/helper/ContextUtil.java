package com.nakashima.application.helper;

import com.nakashima.application.config.Context;
import com.nakashima.application.table.TwitterTable;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;

import static org.apache.spark.sql.functions.lit;

public class ContextUtil implements SparkTest {

    private static String PATH = "src/test/resources/tweet.json";

    public static Context build() {
        Context context = new Context(sparkSession, new TwitterTable());
        Dataset<Row> ds = sparkSession
                .read()
                .text(PATH)
                .withColumn("timestamp", lit(new Timestamp(System.currentTimeMillis())));

        context.getTwitterTable().setDataset(ds);
        return context;
    }

    public static Context build(String jsonStr) {
        Context context = new Context(sparkSession, new TwitterTable());

        List<String> jsonList = Arrays.asList(jsonStr);
        Dataset<String> jsonStrDataset = sparkSession.createDataset(jsonList, Encoders.STRING());
        Dataset<Row> ds = sparkSession.read().json(jsonStrDataset);

        context.getTwitterTable().setDataset(ds);
        return context;
    }
}

package com.nakashima.application.pipeline.dataquality;

import com.nakashima.application.config.Context;
import com.nakashima.application.helper.ContextUtil;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.sql.Timestamp;

import static com.nakashima.application.enumeration.TwitterMessageEnum.CREATED_AT;
import static org.junit.jupiter.api.Assertions.assertEquals;

class FormatDateTest {

    private static Context context;

    private static final String jsonStr = "{ \"createdAt\": \"Apr 29, 2022 1:28:27 PM\", \"user\": {\"createdAt\": \"Feb 6, 2016 5:52:34 AM\"}}";

    @BeforeAll
    static void init() {
        context = ContextUtil.build(jsonStr);
        new FormatDate().process(context);
    }

    @Test
    @Tag("UnitTest")
    @DisplayName("Format date")
    void formatDate() {
        Dataset<Row> ds = context.getTwitterTable().getDataset();

        Timestamp ts = Timestamp.valueOf("2022-04-29 13:28:27");
        assertEquals(ts, ds.select(CREATED_AT.getSchemaField()).first().getTimestamp(0));

        ts = Timestamp.valueOf("2016-02-06 05:52:34");
        assertEquals(ts, ds.select("user.createdAt").first().getTimestamp(0));
    }
}

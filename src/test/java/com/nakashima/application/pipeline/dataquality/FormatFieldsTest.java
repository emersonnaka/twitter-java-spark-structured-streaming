package com.nakashima.application.pipeline.dataquality;

import com.nakashima.application.config.Context;
import com.nakashima.application.helper.ContextUtil;
import com.nakashima.application.pipeline.schema.TwitterSchema;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static com.nakashima.application.enumeration.TwitterMessageEnum.*;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class FormatFieldsTest {

    private static Context context;

    @BeforeAll
    static void init() {
        context = ContextUtil.build();
        new TwitterSchema().process(context);
        new CreatePartitionColumns().process(context);
        new FormatFields().process(context);
    }

    @Test
    @Tag("UnitTest")
    @DisplayName("Format fields from camel case to snake case")
    void formatFieldsFromCamelCaseToSnakeCase() {
        Dataset<Row> ds = context.getTwitterTable().getDataset();
        List<String> columnList = Arrays.asList(ds.columns());

        assertFalse(columnList.contains(CREATED_AT.getSchemaField()));
        assertTrue(columnList.contains(CREATED_AT.getSnakeCaseField()));
        assertTrue(columnList.contains(ID.getSchemaField()));
        assertTrue(columnList.contains(USER_MENTION_ENTITIES.getSnakeCaseField()));

        for (String field : columnList) {
            assertFalse(field.matches("[A-Z]"));
        }

        columnList = Arrays.asList(ds.select("user.*").columns());
        for (String field : columnList) {
            assertFalse(field.matches("[A-Z]"));
        }

    }
}

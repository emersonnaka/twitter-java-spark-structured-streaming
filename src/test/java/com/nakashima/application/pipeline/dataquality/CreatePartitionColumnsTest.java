package com.nakashima.application.pipeline.dataquality;

import com.nakashima.application.config.Context;
import com.nakashima.application.helper.ContextUtil;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import static com.nakashima.application.enumeration.ConstEnum.DAY;
import static com.nakashima.application.enumeration.ConstEnum.MONTH;
import static com.nakashima.application.enumeration.ConstEnum.YEAR;
import static org.junit.jupiter.api.Assertions.assertEquals;

class CreatePartitionColumnsTest {

    private static Context context;

    private static final String jsonStr = "{\"createdAt\": \"2022-04-29 13:28:27\"}";

    @BeforeAll
    static void init() {
        context = ContextUtil.build(jsonStr);
        new CreatePartitionColumns().process(context);
    }

    @Test
    @Tag("UnitTest")
    @DisplayName("Check partition columns")
    void checkPartitionColumns() {
        Dataset<Row> ds = context.getTwitterTable().getDataset();

        assertEquals(2022, ds.select(YEAR.getConstant()).first().getInt(0));
        assertEquals(04, ds.select(MONTH.getConstant()).first().getInt(0));
        assertEquals(29, ds.select(DAY.getConstant()).first().getInt(0));
    }
}

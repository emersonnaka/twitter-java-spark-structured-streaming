package com.nakashima.application.pipeline.schema;

import com.nakashima.application.config.Context;
import com.nakashima.application.helper.ContextUtil;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import static com.nakashima.application.enumeration.TwitterMessageEnum.*;
import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.functions.explode;
import static org.apache.spark.sql.functions.size;
import static org.junit.jupiter.api.Assertions.assertEquals;

class TwitterSchemaTest {

    private static Context context;

    @BeforeAll
    static void init() {
        context = ContextUtil.build();
        new TwitterSchema().process(context);
    }

    @Test
    @Tag("UnitTest")
    @DisplayName("Apply twitter schema")
    void checkApplyTwitterSchema() {
        Dataset<Row> ds = context.getTwitterTable().getDataset();

        assertEquals(1520077564282388480L, ds.select(ID.getSchemaField()).first().getLong(0));
        assertEquals(4880544759L, ds.select("user.id").first().getLong(0));
    }

    @Test
    @Tag("UnitTest")
    @DisplayName("Struct array")
    void checkStructArray() {
        Dataset<Row> ds = context.getTwitterTable().getDataset();

        assertEquals(2, ds.select(size(col(HASHTAG_ENTITIES.getSchemaField()))).first().getInt(0));
        assertEquals(1, ds.select(size(col(USER_MENTION_ENTITIES.getSchemaField()))).first().getInt(0));

        ds = ds.select(explode(ds.col(USER_MENTION_ENTITIES.getSchemaField()))).select("col.*");
        assertEquals(1141663136786743297L, ds.select(ID.getSchemaField()).first().getLong(0));
    }

}

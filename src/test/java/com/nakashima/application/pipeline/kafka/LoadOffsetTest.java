package com.nakashima.application.pipeline.kafka;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static com.nakashima.application.enumeration.ConstEnum.BACKUP;
import static com.nakashima.application.enumeration.ConstEnum.STORAGE;
import static com.nakashima.application.enumeration.PropertiesEnum.SPARK_CHECKPOINT_LOCATION;
import static com.nakashima.common.PropertiesReader.get;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class LoadOffsetTest {

    private static LoadOffset loadOffset;
    @BeforeAll
    static void init() {
        loadOffset = new LoadOffset();
    }

    @Test
    @Tag("UnitTest")
    @DisplayName("Last partition offset json string")
    void lastPartitionOffsetJsonString() {
        assertEquals("{\"twitter\":{\"1\":0,\"0\":0}}", loadOffset.getPartitionOffset());
    }

    @Test
    @Tag("UnitTest")
    @DisplayName("Check backup path created")
    void checkBackupPathCreated() {
        String backupPath = get(SPARK_CHECKPOINT_LOCATION.getValue()) + BACKUP.getConstant();
        Path path = new Path(backupPath);
        try {
            FileSystem fs = FileSystem.get(new Configuration());
            assertTrue(fs.exists(path));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    @Tag("UnitTest")
    @DisplayName("Check offset path moved")
    void checkOffsetPathMoved() {
        String offsetPath = get(SPARK_CHECKPOINT_LOCATION.getValue()) + STORAGE.getConstant() + "/offsets/";
        Path path = new Path(offsetPath);
        try {
            FileSystem fs = FileSystem.get(new Configuration());
            assertFalse(fs.exists(path));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}

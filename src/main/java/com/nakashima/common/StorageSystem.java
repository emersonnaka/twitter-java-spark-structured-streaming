package com.nakashima.common;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class StorageSystem {

    private static FileSystem getFileSystem() {
        try {
            return FileSystem.get(new Configuration());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static boolean pathExists(String strPath) {
        FileSystem fs = getFileSystem();
        Path path = new Path(strPath);
        try {
            return fs.exists(path);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static String getLastFileByModificationTime(String strPath) {
        String filePath = "";
        long lastFileCreated = Long.MIN_VALUE;
        FileSystem fs = getFileSystem();
        Path path = new Path(strPath);

        try {
            for (FileStatus fileStatus : fs.listStatus(path)) {
                if (fileStatus.getModificationTime() > lastFileCreated) {
                    filePath = fileStatus.getPath().toString();
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return filePath;
    }

    public static BufferedReader getContentFile(String filePath) {
        FileSystem fs = getFileSystem();
        Path path = new Path(filePath);
        try {
            return new BufferedReader(new InputStreamReader(fs.open(path)));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static boolean mkdirs(String strPath) {
        FileSystem fs = getFileSystem();
        Path path = new Path(strPath);
        try {
            return fs.mkdirs(path);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void moveFiles(String source, String to) {
        FileSystem fs = getFileSystem();
        Path sourcePath = new Path(source);
        Path toPath = new Path(to);

        try {
            for (FileStatus fileStatus : fs.listStatus(sourcePath)) {
                fs.moveFromLocalFile(fileStatus.getPath(), toPath);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}

package com.nakashima.common;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesReader {

    private static final Properties PROPERTIES = new Properties();

    static {
        InputStream is = PropertiesReader.class.getResourceAsStream("/config.properties");
        try {
            PROPERTIES.load(is);
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public PropertiesReader() {
    }

    public static String get(String key) {
        return PROPERTIES.getProperty(key);
    }

}

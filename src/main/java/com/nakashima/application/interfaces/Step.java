package com.nakashima.application.interfaces;

import com.nakashima.application.config.Context;

public interface Step {

    void process(Context context);

}

package com.nakashima.application.enumeration;

public enum ConstEnum {

    BACKUP("backup"),
    DAY("day"),
    EARLIEST("earliest"),
    MONTH("month"),
    STORAGE("storage"),
    SEP(">"),
    TIMESTAMP_FOMART("MMM d, yyyy h:mm:ss a"),
    YEAR("year");

    private final String constant;

    ConstEnum(String constant) {
        this.constant = constant;
    }

    public String getConstant() {
        return this.constant;
    }

}

package com.nakashima.application.enumeration;

public enum PropertiesEnum {

    FILE_TYPE("file.type"),
    KAFKA_BOOTSTRAP_SERVER("kafka.bootstrap.server"),
    KAFKA_TOPIC("kafka.topic"),
    SPARK_APP_NAME("spark.app.name"),
    SPARK_CHECKPOINT_LOCATION("spark.checkpoint.location"),
    STORAGE_PATH("storage.path");

    private final String value;

    PropertiesEnum(String value) {
        this.value = value;
    }

    public final String getValue() {
        return this.value;
    }

}

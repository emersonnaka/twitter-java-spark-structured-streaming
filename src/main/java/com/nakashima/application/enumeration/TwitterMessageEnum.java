package com.nakashima.application.enumeration;

public enum TwitterMessageEnum {

    CREATED_AT("createdAt", "created_at"),
    ID("id", ""),
    TEXT("text", ""),
    SOURCE("source", ""),
    IS_TRUNCATED("isTruncated", "is_truncated"),
    IN_REPLY_TO_STATUS_ID("inReplyToStatusId", "in_reply_to_status_id"),
    IN_REPLY_TO_USER_ID("inReplyToUserId", "in_reply_to_user_id"),
    IS_FAVORITED("isFavorited", "is_favorited"),
    IS_RETWEETED("isRetweeted", "is_retweeted"),
    FAVORITE_COUNT("favoriteCount", "favorite_count"),
    IN_REPLAY_TO_SCREEN_NAME("inReplyToScreenName", "in_reply_to_screen_name"),
    RETWEET_COUNT("retweetCount", "retweet_count"),
    IS_POSSIBLY_SENSITIVE("isPossiblySensitive", "is_possibly_sensitive"),
    LANG("lang", ""),
    USER_MENTION_ENTITIES("userMentionEntities", "user_mention_entities"),
    NAME("name", ""),
    SCREEN_NAME("screenName", "screen_name"),
    START("start", ""),
    END("end", ""),
    URL_ENTITIES("urlEntities", "url_entities"),
    URL("url", ""),
    EXPANDED_URL("expandedURL", "expanded_url"),
    DISPLAY_URL("displayURL", "display_url"),
    HASHTAG_ENTITIES("hashtagEntities", "hashtag_entities"),
    MEDIA_ENTITIES("mediaEntities", "media_entities"),
    MEDIA_URL("mediaUrl", "media_url"),
    MEDIA_URL_HTTPS("mediaURLHttps", "media_url_https"),
    SIZES("sizes", ""),
    ZERO("0", ""),
    ONE("1", ""),
    TWO("2", ""),
    THREE("3", ""),
    WIDTH("width", ""),
    HEIGHT("height", ""),
    RESIZE("resize", ""),
    TYPE("type", ""),
    VIDEO_ASPECT_RATIO_WIDTH("videoAspectRatioWidth", "video_aspect_ratio_width"),
    VIDEO_ASPECT_RATIO_HEIGHT("videoAspectRatioHeight", "video_aspect_ratio_height"),
    VIDEO_DURATION_MILLIS("videoDurationMillis", "video_duration_millis"),
    VIDEO_VARIANTS("videoVariants", "video_variants"),
    BITRATE("bitrate", ""),
    CONTENT_TYPE("contentType", "content_type"),
    SYMBOL_ENTITIES("symbolEntities", "symbol_entities"),
    INDICES("indices", ""),
    CURRENT_USER_RETWEET_ID("currentUserRetweetId", "current_user_retweet_id"),
    USER("user", ""),
    DESCRIPTION("description", ""),
    DESCRIPTION_URL_ENTITIES("descriptionURLEntities", "description_url_entities"),
    IS_CONTRIBUTORS_ENABLED("isContributorsEnabled", "is_contributors_enabled"),
    PROFILE_IMAGE_URL("profileImageUrl", "profile_image_url"),
    PROFILE_IMAGE_URL_HTTPS("profileImageUrlHttps", "profile_image_url_https"),
    IS_DEFAULT_PROFILE_IMAGE("isDefaultProfileImage", "is_default_profile_image"),
    IS_PROTECTED("isProtected", "is_protected"),
    FOLLOWERS_COUNT("followersCount", "followers_count"),
    PROFILE_BACKGROUND_COLOR("profileBackgroundColor", "profile_background_color"),
    PROFILE_TEXT_COLOR("profileTextColor", "profile_text_color"),
    PROFILE_LINK_COLOR("profileLinkColor", "profile_link_color"),
    PROFILE_SIDEBAR_FILL_COLOR("profileSidebarFillColor", "profile_sidebar_fill_color"),
    PROFILE_SIDEBAR_BORDER_COLOR("profileSidebarBorderColor", "profile_sidebar_border_color"),
    PROFILE_USE_BACKGROUND_IMAGE("profileUseBackgroundImage", "profile_use_background_image"),
    IS_DEFAULT_PROFILE("isDefaultProfile", "is_default_profile"),
    SHOW_ALL_INLINE_MEDIA("showAllInlineMedia", "show_all_inline_media"),
    FRIENDS_COUNT("friendsCount", "friends_count"),
    FAVOURITES_COUNT("favouritesCount", "favourites_count"),
    UTC_OFFSET("utcOffset", "utc_offset"),
    PROFILE_BACKGROUND_IMAGE_URL("profileBackgroundImageUrl", "profile_background_image_url"),
    PROFILE_BACKGROUND_IMAGE_URL_HTTPS("profileBackgroundImageUrlHttps", "profile_background_image_url_https"),
    PROFILE_BANNER_IMAGE_URL("profileBannerImageUrl", "profile_banner_image_url"),
    PROFILE_BACKGROUND_TILED("profileBackgroundTiled", "profile_background_tiled"),
    STATUSES_COUNT("statusesCount", "statuses_count"),
    IS_GEO_ENABLED("isGeoEnabled", "is_geo_enabled"),
    IS_VERIFIED("isVerified", "is_verified"),
    TRANSLATOR("translator", ""),
    LISTED_COUNT("listedCount", "listed_count"),
    IS_FOLLOW_REQUEST_SENT("isFollowRequestSent", "is_follow_request_sent"),
    QUOTED_STATUS_ID("quotedStatusId", "quoted_status_id");

    private final String schemaField;

    private final String snakeCaseField;

    TwitterMessageEnum(String schemaField, String snakeCaseField) {
        this.schemaField = schemaField;
        this.snakeCaseField = snakeCaseField;
    }

    public String getSchemaField() {
        return schemaField;
    }

    public String getSnakeCaseField() {
        return snakeCaseField;
    }

}

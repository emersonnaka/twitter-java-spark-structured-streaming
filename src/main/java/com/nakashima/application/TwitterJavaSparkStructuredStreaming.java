package com.nakashima.application;

import com.nakashima.application.config.Context;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.streaming.StreamingQueryException;

import static com.nakashima.application.enumeration.PropertiesEnum.SPARK_APP_NAME;
import static com.nakashima.application.enumeration.PropertiesEnum.SPARK_CHECKPOINT_LOCATION;
import static com.nakashima.common.PropertiesReader.get;

public class TwitterJavaSparkStructuredStreaming {

    private static SparkSession createSparkSession() {
        return SparkSession
                .builder()
                .appName(get(SPARK_APP_NAME.getValue()))
                .master("local")
                .config("spark.sql.streaming.checkpointLocation", get(SPARK_CHECKPOINT_LOCATION.getValue()))
                .getOrCreate();
    }
    
    public static void main( String[] args ) throws StreamingQueryException {
        SparkSession sparkSession = createSparkSession();
        sparkSession.sparkContext().setLogLevel("ERROR");
        new Context(sparkSession);
        sparkSession.streams().awaitAnyTermination();
    }
}

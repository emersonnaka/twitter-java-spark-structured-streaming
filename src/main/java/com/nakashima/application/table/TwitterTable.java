package com.nakashima.application.table;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.types.StructType;

import static com.nakashima.application.enumeration.TwitterMessageEnum.*;
import static org.apache.spark.sql.types.DataTypes.createArrayType;
import static org.apache.spark.sql.types.DataTypes.BooleanType;
import static org.apache.spark.sql.types.DataTypes.IntegerType;
import static org.apache.spark.sql.types.DataTypes.LongType;
import static org.apache.spark.sql.types.DataTypes.StringType;

public class TwitterTable {

    private Dataset<Row> dataset;

    private StructType schema;

    public TwitterTable() {
        this.schema = createSchema();
    }

    private StructType createSchema() {
        return new StructType()
                .add(CREATED_AT.getSchemaField(), StringType, false)
                .add(ID.getSchemaField(), LongType, false)
                .add(TEXT.getSchemaField(), StringType, false)
                .add(SOURCE.getSchemaField(), StringType, false)
                .add(IS_TRUNCATED.getSchemaField(), BooleanType, false)
                .add(IN_REPLY_TO_STATUS_ID.getSchemaField(), LongType, false)
                .add(IN_REPLY_TO_USER_ID.getSchemaField(), LongType, false)
                .add(IS_FAVORITED.getSchemaField(), BooleanType, false)
                .add(IS_RETWEETED.getSchemaField(), BooleanType, false)
                .add(FAVORITE_COUNT.getSchemaField(), IntegerType, false)
                .add(IN_REPLAY_TO_SCREEN_NAME.getSchemaField(), StringType, false)
                .add(RETWEET_COUNT.getSchemaField(), IntegerType, false)
                .add(IS_POSSIBLY_SENSITIVE.getSchemaField(), BooleanType, false)
                .add(LANG.getSchemaField(), StringType, false)
                .add(USER_MENTION_ENTITIES.getSchemaField(), createArrayType(getUserMentionSchema()), true)
                .add(URL_ENTITIES.getSchemaField(), createArrayType(getUrlSchema()), true)
                .add(HASHTAG_ENTITIES.getSchemaField(), createArrayType(getHashtagSchema()), true)
                .add(MEDIA_ENTITIES.getSchemaField(), createArrayType(getMediaSchema()), true)
                .add(SYMBOL_ENTITIES.getSchemaField(), createArrayType(getSymbolSchema()), true)
                .add(CURRENT_USER_RETWEET_ID.getSchemaField(), IntegerType, true)
                .add(USER.getSchemaField(), getUserSchema(), true)
                .add(QUOTED_STATUS_ID.getSchemaField(), IntegerType, true);
    }

    private StructType getUserMentionSchema() {
        return new StructType()
                .add(NAME.getSchemaField(), StringType, true)
                .add(SCREEN_NAME.getSchemaField(), StringType, true)
                .add(ID.getSchemaField(), LongType, true)
                .add(START.getSchemaField(), IntegerType, true)
                .add(END.getSchemaField(), IntegerType, true);
    }

    private StructType getUrlSchema() {
        return new StructType()
                .add(URL.getSchemaField(), StringType, true)
                .add(EXPANDED_URL.getSchemaField(), StringType, true)
                .add(DISPLAY_URL.getSchemaField(), StringType, true)
                .add(START.getSchemaField(), IntegerType, true)
                .add(END.getSchemaField(), IntegerType, true);
    }

    private StructType getHashtagSchema() {
        return new StructType()
                .add(TEXT.getSchemaField(), StringType, true)
                .add(START.getSchemaField(), IntegerType, true)
                .add(END.getSchemaField(), IntegerType, true);
    }

    private StructType getMediaSchema() {
        return new StructType()
                .add(ID.getSchemaField(), LongType, true)
                .add(URL.getSchemaField(), StringType, true)
                .add(MEDIA_URL.getSchemaField(), StringType, true)
                .add(MEDIA_URL_HTTPS.getSchemaField(), StringType, true)
                .add(EXPANDED_URL.getSchemaField(), StringType, true)
                .add(DISPLAY_URL.getSchemaField(), StringType, true)
                .add(SIZES.getSchemaField(), getSizesSchema(), true)
                .add(TYPE.getSchemaField(), StringType, true)
                .add(VIDEO_ASPECT_RATIO_WIDTH.getSchemaField(), IntegerType, true)
                .add(VIDEO_ASPECT_RATIO_HEIGHT.getSchemaField(), IntegerType, true)
                .add(VIDEO_DURATION_MILLIS.getSchemaField(), IntegerType, true)
                .add(VIDEO_VARIANTS.getSchemaField(), createArrayType(getVideoVariantsSchema()), true)
                .add(START.getSchemaField(), IntegerType, true)
                .add(END.getSchemaField(), IntegerType, true);
    }

    private StructType getSizesSchema() {
        return new StructType()
                .add(ZERO.getSchemaField(), getSizeSchema(), true)
                .add(ONE.getSchemaField(), getSizeSchema(), true)
                .add(TWO.getSchemaField(), getSizeSchema(), true)
                .add(THREE.getSchemaField(), getSizeSchema(), true);
    }

    private StructType getSizeSchema() {
        return new StructType()
                .add(WIDTH.getSchemaField(), IntegerType, true)
                .add(HEIGHT.getSchemaField(), IntegerType, true)
                .add(RESIZE.getSchemaField(), IntegerType, true);
    }

    private StructType getVideoVariantsSchema() {
        return new StructType()
                .add(BITRATE.getSchemaField(), IntegerType, true)
                .add(CONTENT_TYPE.getSchemaField(), StringType, true)
                .add(URL.getSchemaField(), StringType, true);
    }

    private StructType getSymbolSchema() {
        return new StructType()
                .add(INDICES.getSchemaField(), createArrayType(IntegerType), true)
                .add(TEXT.getSchemaField(), StringType, true);
    }

    private StructType getUserSchema() {
        return new StructType()
                .add(ID.getSchemaField(), LongType, true)
                .add(NAME.getSchemaField(), StringType, true)
                .add(SCREEN_NAME.getSchemaField(), StringType, true)
                .add(DESCRIPTION.getSchemaField(), StringType, true)
                .add(DESCRIPTION_URL_ENTITIES.getSchemaField(), createArrayType(getDescriptionUrlSchema()), true)
                .add(IS_CONTRIBUTORS_ENABLED.getSchemaField(), BooleanType, true)
                .add(PROFILE_IMAGE_URL.getSchemaField(), StringType, true)
                .add(PROFILE_IMAGE_URL_HTTPS.getSchemaField(), StringType, true)
                .add(IS_DEFAULT_PROFILE_IMAGE.getSchemaField(), BooleanType, true)
                .add(IS_PROTECTED.getSchemaField(), BooleanType, true)
                .add(FOLLOWERS_COUNT.getSchemaField(), IntegerType, true)
                .add(PROFILE_BACKGROUND_COLOR.getSchemaField(), StringType, true)
                .add(PROFILE_TEXT_COLOR.getSchemaField(), StringType, true)
                .add(PROFILE_LINK_COLOR.getSchemaField(), StringType, true)
                .add(PROFILE_SIDEBAR_FILL_COLOR.getSchemaField(), StringType, true)
                .add(PROFILE_SIDEBAR_BORDER_COLOR.getSchemaField(), StringType, true)
                .add(PROFILE_USE_BACKGROUND_IMAGE.getSchemaField(), BooleanType, true)
                .add(IS_DEFAULT_PROFILE.getSchemaField(), BooleanType, true)
                .add(SHOW_ALL_INLINE_MEDIA.getSchemaField(), BooleanType, true)
                .add(FRIENDS_COUNT.getSchemaField(), IntegerType, true)
                .add(CREATED_AT.getSchemaField(), StringType, true)
                .add(FAVOURITES_COUNT.getSchemaField(), IntegerType, true)
                .add(UTC_OFFSET.getSchemaField(), IntegerType, true)
                .add(PROFILE_BACKGROUND_IMAGE_URL.getSchemaField(), StringType, true)
                .add(PROFILE_BACKGROUND_IMAGE_URL_HTTPS.getSchemaField(), StringType, true)
                .add(PROFILE_BANNER_IMAGE_URL.getSchemaField(), StringType, true)
                .add(PROFILE_BACKGROUND_TILED.getSchemaField(), BooleanType, true)
                .add(STATUSES_COUNT.getSchemaField(), IntegerType, true)
                .add(IS_GEO_ENABLED.getSchemaField(), BooleanType, true)
                .add(IS_VERIFIED.getSchemaField(), BooleanType, true)
                .add(TRANSLATOR.getSchemaField(), BooleanType, true)
                .add(LISTED_COUNT.getSchemaField(), IntegerType, true)
                .add(IS_FOLLOW_REQUEST_SENT.getSchemaField(), BooleanType, true);
    }

    private StructType getDescriptionUrlSchema() {
        return new StructType()
                .add(URL.getSchemaField(), StringType, true)
                .add(EXPANDED_URL.getSchemaField(), StringType, true)
                .add(DISPLAY_URL.getSchemaField(), StringType, true);
    }

    public Dataset<Row> getDataset() {
        return dataset;
    }

    public void setDataset(Dataset<Row> dataset) {
        this.dataset = dataset;
    }

    public StructType getSchema() {
        return schema;
    }

}

package com.nakashima.application.pipeline.save;

import com.nakashima.application.config.Context;
import com.nakashima.application.interfaces.Step;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.streaming.OutputMode;
import org.apache.spark.sql.streaming.Trigger;

import java.util.concurrent.TimeoutException;

import static com.nakashima.application.enumeration.ConstEnum.DAY;
import static com.nakashima.application.enumeration.ConstEnum.MONTH;
import static com.nakashima.application.enumeration.ConstEnum.STORAGE;
import static com.nakashima.application.enumeration.ConstEnum.YEAR;
import static com.nakashima.application.enumeration.PropertiesEnum.FILE_TYPE;
import static com.nakashima.application.enumeration.PropertiesEnum.STORAGE_PATH;
import static com.nakashima.application.enumeration.TwitterMessageEnum.LANG;
import static com.nakashima.common.PropertiesReader.get;

public class SaveStorage implements Step {


    @Override
    public void process(Context context) {
        Dataset<Row> ds = context.getTwitterTable().getDataset();

        try {
            ds.writeStream()
                    .queryName(STORAGE.getConstant())
                    .outputMode(OutputMode.Append())
                    .format(get(FILE_TYPE.getValue()))
                    .option("path", get(STORAGE_PATH.getValue()))
                    .partitionBy(
                            LANG.getSchemaField(),
                            YEAR.getConstant(),
                            MONTH.getConstant(),
                            DAY.getConstant()
                    )
                    .trigger(Trigger.ProcessingTime("30 seconds"))
                    .start();
        } catch (TimeoutException e) {
            throw new RuntimeException(e);
        }
    }
}

package com.nakashima.application.pipeline.dataquality;

import com.nakashima.application.config.Context;
import com.nakashima.application.interfaces.Step;
import com.nakashima.application.table.TwitterTable;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

import static com.nakashima.application.enumeration.ConstEnum.DAY;
import static com.nakashima.application.enumeration.ConstEnum.MONTH;
import static com.nakashima.application.enumeration.ConstEnum.YEAR;
import static com.nakashima.application.enumeration.TwitterMessageEnum.CREATED_AT;
import static org.apache.spark.sql.functions.dayofmonth;
import static org.apache.spark.sql.functions.month;
import static org.apache.spark.sql.functions.year;

public class CreatePartitionColumns implements Step {

    @Override
    public void process(Context context) {
        TwitterTable twitterTable = context.getTwitterTable();
        Dataset<Row> ds = context.getTwitterTable().getDataset();

        ds = ds.withColumn(YEAR.getConstant(), year(ds.col(CREATED_AT.getSchemaField())))
                .withColumn(MONTH.getConstant(), month(ds.col(CREATED_AT.getSchemaField())))
                .withColumn(DAY.getConstant(), dayofmonth(ds.col(CREATED_AT.getSchemaField())));

        twitterTable.setDataset(ds);
    }
}

package com.nakashima.application.pipeline.dataquality;

import com.nakashima.application.config.Context;
import com.nakashima.application.interfaces.Step;
import com.nakashima.application.table.TwitterTable;
import org.apache.spark.sql.Column;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

import static com.nakashima.application.enumeration.ConstEnum.SEP;
import static com.nakashima.application.enumeration.ConstEnum.TIMESTAMP_FOMART;
import static com.nakashima.application.enumeration.TwitterMessageEnum.CREATED_AT;
import static com.nakashima.application.enumeration.TwitterMessageEnum.USER;
import static org.apache.spark.sql.functions.to_timestamp;

public class FormatDate implements Step {

    @Override
    public void process(Context context) {
        TwitterTable twitterTable = context.getTwitterTable();
        Dataset<Row> ds = context.getTwitterTable().getDataset();
        String userCreatedAt = USER.getSchemaField() + SEP.getConstant() + CREATED_AT.getSchemaField();

        ds = formatDate(ds, CREATED_AT.getSchemaField(), userCreatedAt);

        twitterTable.setDataset(ds);
    }

    private Dataset<Row> formatDate(Dataset<Row> ds, String... fields) {
        for (String field : fields) {
            if (field.contains(SEP.getConstant())) {
                String[] fieldArray = field.split(SEP.getConstant());
                String correctField = fieldArray[0] + "." + fieldArray[1];
                ds = ds.withColumn(
                        fieldArray[0],
                        ds.col(fieldArray[0]).withField(
                                fieldArray[1],
                                castColumnToTimestamp(ds, correctField)));
            } else {
                ds = ds.withColumn(
                        field,
                        castColumnToTimestamp(ds, field));
            }
        }

        return ds;
    }

    private Column castColumnToTimestamp(Dataset<Row> ds, String field) {
        return to_timestamp(ds.col(field), TIMESTAMP_FOMART.getConstant());
    }

}

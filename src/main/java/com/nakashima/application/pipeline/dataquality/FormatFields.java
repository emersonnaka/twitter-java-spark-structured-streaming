package com.nakashima.application.pipeline.dataquality;

import com.nakashima.application.config.Context;
import com.nakashima.application.interfaces.Step;
import com.nakashima.application.table.TwitterTable;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.types.ArrayType;
import org.apache.spark.sql.types.StructType;

import static com.nakashima.application.enumeration.ConstEnum.DAY;
import static com.nakashima.application.enumeration.ConstEnum.MONTH;
import static com.nakashima.application.enumeration.ConstEnum.YEAR;
import static com.nakashima.application.enumeration.TwitterMessageEnum.*;
import static org.apache.spark.sql.types.DataTypes.*;
import static org.apache.spark.sql.types.DataTypes.IntegerType;

public class FormatFields implements Step {

    @Override
    public void process(Context context) {
        TwitterTable twitterTable = context.getTwitterTable();
        Dataset<Row> ds = context.getTwitterTable().getDataset();

        ds = renameArrayFields(ds);
        ds = formatFields(ds);

        twitterTable.setDataset(ds);
    }

    private Dataset<Row> renameArrayFields(Dataset<Row> ds) {
        return ds
                .withColumnRenamed(USER_MENTION_ENTITIES.getSchemaField(), USER_MENTION_ENTITIES.getSnakeCaseField())
                .withColumnRenamed(URL_ENTITIES.getSchemaField(), URL_ENTITIES.getSnakeCaseField())
                .withColumnRenamed(HASHTAG_ENTITIES.getSchemaField(), HASHTAG_ENTITIES.getSnakeCaseField())
                .withColumnRenamed(MEDIA_ENTITIES.getSchemaField(), MEDIA_ENTITIES.getSnakeCaseField())
                .withColumnRenamed(SYMBOL_ENTITIES.getSchemaField(), SYMBOL_ENTITIES.getSnakeCaseField());
    }

    private Dataset<Row> formatFields(Dataset<Row> ds) {
        return ds.select(
                ds.col(CREATED_AT.getSchemaField()).as(CREATED_AT.getSnakeCaseField()),
                ds.col(ID.getSchemaField()),
                ds.col(TEXT.getSchemaField()),
                ds.col(SOURCE.getSchemaField()),
                ds.col(IS_TRUNCATED.getSchemaField()).as(IS_TRUNCATED.getSnakeCaseField()),
                ds.col(IN_REPLY_TO_STATUS_ID.getSchemaField()).as(IN_REPLY_TO_STATUS_ID.getSnakeCaseField()),
                ds.col(IN_REPLY_TO_USER_ID.getSchemaField()).as(IN_REPLY_TO_USER_ID.getSnakeCaseField()),
                ds.col(IS_FAVORITED.getSchemaField()).as(IS_FAVORITED.getSnakeCaseField()),
                ds.col(IS_RETWEETED.getSchemaField()).as(IS_RETWEETED.getSnakeCaseField()),
                ds.col(FAVORITE_COUNT.getSchemaField()).as(FAVORITE_COUNT.getSnakeCaseField()),
                ds.col(IN_REPLAY_TO_SCREEN_NAME.getSchemaField()).as(IN_REPLAY_TO_SCREEN_NAME.getSnakeCaseField()),
                ds.col(RETWEET_COUNT.getSchemaField()).as(RETWEET_COUNT.getSnakeCaseField()),
                ds.col(IS_POSSIBLY_SENSITIVE.getSchemaField()).as(IS_POSSIBLY_SENSITIVE.getSnakeCaseField()),
                ds.col(LANG.getSchemaField()),
                ds.col(USER_MENTION_ENTITIES.getSnakeCaseField()).cast(renameUserMentionArray()),
                ds.col(URL_ENTITIES.getSnakeCaseField()).cast(renameUrlSchema()),
                ds.col(HASHTAG_ENTITIES.getSnakeCaseField()),
                ds.col(MEDIA_ENTITIES.getSnakeCaseField()).cast(renameMediaSchema()),
                ds.col(SYMBOL_ENTITIES.getSnakeCaseField()),
                ds.col(USER.getSchemaField()).cast(renameUserSchema()),
                ds.col(QUOTED_STATUS_ID.getSchemaField()).as(QUOTED_STATUS_ID.getSnakeCaseField()),
                ds.col(YEAR.getConstant()),
                ds.col(MONTH.getConstant()),
                ds.col(DAY.getConstant())
        );
    }

    private ArrayType renameUserMentionArray() {
        return createArrayType(
                new StructType()
                        .add(NAME.getSchemaField(), StringType, true)
                        .add(SCREEN_NAME.getSnakeCaseField(), StringType, true)
                        .add(ID.getSchemaField(), LongType, true)
                        .add(START.getSchemaField(), IntegerType, true)
                        .add(END.getSchemaField(), IntegerType, true)
        );
    }

    private ArrayType renameUrlSchema() {
        return createArrayType(
                new StructType()
                        .add(URL.getSchemaField(), StringType, true)
                        .add(EXPANDED_URL.getSnakeCaseField(), StringType, true)
                        .add(DISPLAY_URL.getSnakeCaseField(), StringType, true)
                        .add(START.getSchemaField(), IntegerType, true)
                        .add(END.getSchemaField(), IntegerType, true)
        );
    }

    private ArrayType renameMediaSchema() {
        return createArrayType(
                new StructType()
                        .add(ID.getSchemaField(), LongType, true)
                        .add(URL.getSchemaField(), StringType, true)
                        .add(MEDIA_URL.getSnakeCaseField(), StringType, true)
                        .add(MEDIA_URL_HTTPS.getSnakeCaseField(), StringType, true)
                        .add(EXPANDED_URL.getSnakeCaseField(), StringType, true)
                        .add(DISPLAY_URL.getSnakeCaseField(), StringType, true)
                        .add(SIZES.getSchemaField(), getSizesSchema(), true)
                        .add(TYPE.getSchemaField(), StringType, true)
                        .add(VIDEO_ASPECT_RATIO_WIDTH.getSnakeCaseField(), IntegerType, true)
                        .add(VIDEO_ASPECT_RATIO_HEIGHT.getSnakeCaseField(), IntegerType, true)
                        .add(VIDEO_DURATION_MILLIS.getSnakeCaseField(), IntegerType, true)
                        .add(VIDEO_VARIANTS.getSnakeCaseField(), renameVideoVariantsSchema(), true)
                        .add(START.getSchemaField(), IntegerType, true)
                        .add(END.getSchemaField(), IntegerType, true)
        );
    }

    private StructType getSizesSchema() {
        return new StructType()
                .add(ZERO.getSchemaField(), getSizeSchema(), true)
                .add(ONE.getSchemaField(), getSizeSchema(), true)
                .add(TWO.getSchemaField(), getSizeSchema(), true)
                .add(THREE.getSchemaField(), getSizeSchema(), true);
    }

    private StructType getSizeSchema() {
        return new StructType()
                .add(WIDTH.getSchemaField(), IntegerType, true)
                .add(HEIGHT.getSchemaField(), IntegerType, true)
                .add(RESIZE.getSchemaField(), IntegerType, true);
    }

    private ArrayType renameVideoVariantsSchema() {
        return createArrayType(
                new StructType()
                        .add(BITRATE.getSchemaField(), IntegerType, true)
                        .add(CONTENT_TYPE.getSnakeCaseField(), StringType, true)
                        .add(URL.getSchemaField(), StringType, true)
        );
    }

    private StructType renameUserSchema() {
        return new StructType()
                .add(ID.getSchemaField(), LongType, true)
                .add(NAME.getSchemaField(), StringType, true)
                .add(SCREEN_NAME.getSnakeCaseField(), StringType, true)
                .add(DESCRIPTION.getSchemaField(), StringType, true)
                .add(DESCRIPTION_URL_ENTITIES.getSnakeCaseField(), renameDescriptionUrlSchema(), true)
                .add(IS_CONTRIBUTORS_ENABLED.getSnakeCaseField(), BooleanType, true)
                .add(PROFILE_IMAGE_URL.getSnakeCaseField(), StringType, true)
                .add(PROFILE_IMAGE_URL_HTTPS.getSnakeCaseField(), StringType, true)
                .add(IS_DEFAULT_PROFILE_IMAGE.getSnakeCaseField(), BooleanType, true)
                .add(IS_PROTECTED.getSnakeCaseField(), BooleanType, true)
                .add(FOLLOWERS_COUNT.getSnakeCaseField(), IntegerType, true)
                .add(PROFILE_BACKGROUND_COLOR.getSnakeCaseField(), StringType, true)
                .add(PROFILE_TEXT_COLOR.getSnakeCaseField(), StringType, true)
                .add(PROFILE_LINK_COLOR.getSnakeCaseField(), StringType, true)
                .add(PROFILE_SIDEBAR_FILL_COLOR.getSnakeCaseField(), StringType, true)
                .add(PROFILE_SIDEBAR_BORDER_COLOR.getSnakeCaseField(), StringType, true)
                .add(PROFILE_USE_BACKGROUND_IMAGE.getSnakeCaseField(), BooleanType, true)
                .add(IS_DEFAULT_PROFILE.getSnakeCaseField(), BooleanType, true)
                .add(SHOW_ALL_INLINE_MEDIA.getSnakeCaseField(), BooleanType, true)
                .add(FRIENDS_COUNT.getSnakeCaseField(), IntegerType, true)
                .add(CREATED_AT.getSnakeCaseField(), StringType, true)
                .add(FAVOURITES_COUNT.getSnakeCaseField(), IntegerType, true)
                .add(UTC_OFFSET.getSnakeCaseField(), IntegerType, true)
                .add(PROFILE_BACKGROUND_IMAGE_URL.getSnakeCaseField(), StringType, true)
                .add(PROFILE_BACKGROUND_IMAGE_URL_HTTPS.getSnakeCaseField(), StringType, true)
                .add(PROFILE_BANNER_IMAGE_URL.getSnakeCaseField(), StringType, true)
                .add(PROFILE_BACKGROUND_TILED.getSnakeCaseField(), BooleanType, true)
                .add(STATUSES_COUNT.getSnakeCaseField(), IntegerType, true)
                .add(IS_GEO_ENABLED.getSnakeCaseField(), BooleanType, true)
                .add(IS_VERIFIED.getSnakeCaseField(), BooleanType, true)
                .add(TRANSLATOR.getSchemaField(), BooleanType, true)
                .add(LISTED_COUNT.getSnakeCaseField(), IntegerType, true)
                .add(IS_FOLLOW_REQUEST_SENT.getSnakeCaseField(), BooleanType, true);
    }

    private ArrayType renameDescriptionUrlSchema() {
        return createArrayType(
                new StructType()
                        .add(URL.getSchemaField(), StringType, true)
                        .add(EXPANDED_URL.getSnakeCaseField(), StringType, true)
                        .add(DISPLAY_URL.getSnakeCaseField(), StringType, true));
    }

}

package com.nakashima.application.pipeline.schema;

import com.nakashima.application.config.Context;
import com.nakashima.application.interfaces.Step;
import com.nakashima.application.table.TwitterTable;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

import java.util.concurrent.TimeoutException;

import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.functions.from_json;

public class TwitterSchema implements Step {

    @Override
    public void process(Context context) {
        TwitterTable twitterTable = context.getTwitterTable();
        Dataset<Row> rawDataset = context.getTwitterTable().getDataset();

        Dataset<Row> twitterDataset = rawDataset.select(
                from_json(
                        col("value").cast("string")
                        , twitterTable.getSchema()
                ).as("twitter"));

        twitterDataset = twitterDataset.select("twitter.*");

        twitterTable.setDataset(twitterDataset);
    }

}

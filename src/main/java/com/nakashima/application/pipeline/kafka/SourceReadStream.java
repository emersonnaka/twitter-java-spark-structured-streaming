package com.nakashima.application.pipeline.kafka;

import com.nakashima.application.config.Context;
import com.nakashima.application.interfaces.Step;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

import static com.nakashima.application.enumeration.PropertiesEnum.KAFKA_BOOTSTRAP_SERVER;
import static com.nakashima.application.enumeration.PropertiesEnum.KAFKA_TOPIC;
import static com.nakashima.common.PropertiesReader.get;

public class SourceReadStream implements Step {

    @Override
    public void process(Context context) {
        Dataset<Row> dataset = context.getSparkSession()
                .readStream()
                .format("kafka")
                .option("kafka.bootstrap.servers", get(KAFKA_BOOTSTRAP_SERVER.getValue()))
                .option("subscribe", get(KAFKA_TOPIC.getValue()))
                .option("startingOffsets", context.getPartitionOffset())
                .option("failOnDataLoss", false)
                .load();

        context.getTwitterTable().setDataset(dataset);
    }

}

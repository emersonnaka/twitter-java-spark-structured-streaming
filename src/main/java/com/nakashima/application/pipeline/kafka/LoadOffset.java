package com.nakashima.application.pipeline.kafka;

import com.nakashima.common.StorageSystem;

import java.io.BufferedReader;
import java.io.IOException;
import java.sql.Timestamp;

import static com.nakashima.application.enumeration.ConstEnum.BACKUP;
import static com.nakashima.application.enumeration.ConstEnum.EARLIEST;
import static com.nakashima.application.enumeration.ConstEnum.STORAGE;
import static com.nakashima.application.enumeration.PropertiesEnum.SPARK_CHECKPOINT_LOCATION;
import static com.nakashima.common.PropertiesReader.get;

public class LoadOffset {

    private final String checkpointPath;
    private final String partitionOffset;

    private final String REGEX = "^\\{\\\"[a-zA-Z_\\-]+\\\":\\{\\\"\\d+\\\":\\s*\\d+(,\\s*\\\"\\d+\\\":\\d+)*\\}\\}$";

    public LoadOffset() {
        this.checkpointPath = get(SPARK_CHECKPOINT_LOCATION.getValue()) + STORAGE.getConstant();
        this.partitionOffset = readCheckpointOffset();

        if (!this.partitionOffset.equals(EARLIEST.getConstant())) {
            moveCheckpointToBackup();
        }
    }

    private String readCheckpointOffset() {
        String json = EARLIEST.getConstant();
        if (!StorageSystem.pathExists(this.checkpointPath)) {
            return json;
        }

        String offsetFilePath = getLastOffsetFilePath();
        json = getPartitionOffsetJson(offsetFilePath);
        return json;
    }

    private String getLastOffsetFilePath() {
        String offsetPath = this.checkpointPath + "/offsets/";

        return StorageSystem.getLastFileByModificationTime(offsetPath);
    }

    private String getPartitionOffsetJson(String filePath) {
        String json = EARLIEST.getConstant();
        try {
            BufferedReader br = StorageSystem.getContentFile(filePath);
            String line = br.readLine();
            while (line != null) {
                if (line.matches(REGEX)) {
                    json = line;
                }
                line = br.readLine();
            }

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return json;
    }

    private void moveCheckpointToBackup() {
        String backupStrPath = getBackupPath();
        if (StorageSystem.mkdirs(backupStrPath)) {
            StorageSystem.moveFiles(this.checkpointPath, backupStrPath);
        }
    }

    private String getBackupPath() {
        String execTime = new Timestamp(System.currentTimeMillis())
                .toString()
                .substring(0, 16)
                .replace(" ", "_")
                .replace(":", "_")
                .replace("-", "_");
        String backupPath = get(SPARK_CHECKPOINT_LOCATION.getValue()) + BACKUP.getConstant() + "/" + execTime + "/";
        return backupPath;
    }

    public String getPartitionOffset() {
        return partitionOffset;
    }

}

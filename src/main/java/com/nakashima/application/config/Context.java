package com.nakashima.application.config;

import com.nakashima.application.interfaces.Step;
import com.nakashima.application.pipeline.kafka.LoadOffset;
import com.nakashima.application.table.TwitterTable;
import org.apache.spark.sql.SparkSession;

public class Context {

    private SparkSession sparkSession;

    private TwitterTable twitterTable;

    private ProcessPipeline processPipeline;

    private LoadOffset loadOffset;

    public Context(SparkSession sparkSession) {
        this.sparkSession = sparkSession;
        this.twitterTable = new TwitterTable();
        this.processPipeline = new ProcessPipeline();
        this.loadOffset = new LoadOffset();
        runStepList();
    }

    public Context(SparkSession sparkSession, TwitterTable twitterTable) {
        this.sparkSession = sparkSession;
        this.twitterTable = twitterTable;
    }

    private void runStepList() {
        for (Step step : this.processPipeline.getStepList()) {
            step.process(this);
        }
    }

    public SparkSession getSparkSession() {
        return sparkSession;
    }

    public TwitterTable getTwitterTable() {
        return twitterTable;
    }

    public String getPartitionOffset() {
        return this.loadOffset.getPartitionOffset();
    }

}

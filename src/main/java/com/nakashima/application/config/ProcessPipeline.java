package com.nakashima.application.config;

import com.nakashima.application.interfaces.Step;
import com.nakashima.application.pipeline.kafka.SourceReadStream;
import com.nakashima.application.pipeline.dataquality.CreatePartitionColumns;
import com.nakashima.application.pipeline.dataquality.FormatDate;
import com.nakashima.application.pipeline.dataquality.FormatFields;
import com.nakashima.application.pipeline.save.SaveStorage;
import com.nakashima.application.pipeline.schema.TwitterSchema;

import java.util.Arrays;
import java.util.List;

public class ProcessPipeline {

    private List<Step> stepList;

    public ProcessPipeline() {
        this.stepList = buildStepList();
    }

    private List<Step> buildStepList() {
        return Arrays.asList(
                new SourceReadStream(),
                new TwitterSchema(),
                new FormatDate(),
                new CreatePartitionColumns(),
                new FormatFields(),
                new SaveStorage()
        );
    }

    public List<Step> getStepList() {
        return stepList;
    }

}
